import React from 'react';

import logo1 from '../../img/logo_one.png';
import logo2 from '../../img/logo_two.png';
import logo3 from '../../img/logo_three.png';

import './LogoSelector.css';

const LOGOS = [logo1, logo2, logo3];

export default class LogoSelector extends React.Component {
    render() {
        return (
            <div className="LogoSelector">
                <h2>Add logo</h2>
                <ul>
                    { LOGOS.map(logo => (
                        <li key={logo}>
                            <img
                                src={logo}
                                onClick={this.onSelectImg}
                                draggable
                                onDragStart={this.onDragStart}
                                alt="logo"
                            />
                        </li>
                    )) }
                </ul>
            </div>
        );
    }
    onSelectImg = (e) => {
        this.props.onCreateLogo(e.currentTarget.src);
    };
    onDragStart = (e) => {
        e.dataTransfer.setData('src', e.currentTarget.src);
    };
}