import React from 'react';

import './BackgroundSelector.css';

const backgrounds = [
    '/backgrounds/1.jpg',
    '/backgrounds/2.jpg',
    '/backgrounds/3.jpg',
    '/backgrounds/4.jpg',
];

export default class BackgroundSelector extends React.Component {
    render() {
        const {value} = this.props;
        return (
            <div className="BackgroundSelector">
                <h2>Select background</h2>
                <ul>
                    {backgrounds.map((url, i) => (
                        <li key={i} className={url === value ? 'active' : ''}>
                            <img src={url} onClick={this.onClick} alt="background"/>
                        </li>
                    ))}
                </ul>
                <button type="button" onClick={this.onClick}>Delete background</button>
            </div>
        );
    }
    onClick = (e) => {
        this.props.onChange(e.target.getAttribute('src'));
    };
}