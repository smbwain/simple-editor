import React from 'react';

import {Image} from 'react-konva';

export default class LazyImage extends React.Component {
    state = {
        loadingImg: null,
        img: null,
    };
    componentDidMount() {
        this.imageSrcWasSet();
    }
    componentDidUpdate(prevProps) {
        if (this.props.src !== prevProps.src) {
            this.imageSrcWasSet();
        }
    }
    imageSrcWasSet() {
        if (!this.props.src) {
            this.setState({
                loadingImg: null,
                img: null,
            });
            return;
        }
        const img = new window.Image();
        img.crossOrigin = 'anonymous';
        img.src = this.props.src;
        img.onload = () => {
            if (img === this.state.loadingImg) {
                this.setState({
                    loadingImg: null,
                    img,
                });
            }
        };
        this.setState({
            loadingImg: img,
            img: null,
        });
    }
    render() {
        const {...rest} = this.props;
        const {img} = this.state;
        return img ? (
            <Image image={img} {...rest}/>
        ) : null;
    }
}