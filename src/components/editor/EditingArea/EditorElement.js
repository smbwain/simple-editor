import React from 'react';
import {Text} from 'react-konva';

import LazyImage from './LazyImage';

const EditorElement = ({object, dragging, resizing}) => {
    const x = dragging ? dragging.x : object.x;
    const y = dragging ? dragging.y : object.y;
    if (object.type === 'logo') {
        return (
            <LazyImage
                x={x}
                y={y}
                width={resizing ? object.w * resizing.coef : object.w}
                height={resizing ? object.h * resizing.coef : object.h}
                src={object.imgUrl}
                data-id={object.id}
            />
        );
    }
    return (
        <Text
            x={x}
            y={y}
            text={object.text}
            fontSize={20}
            fontFamily={object.font}
            data-id={object.id}
        />
    );
};

export default EditorElement;