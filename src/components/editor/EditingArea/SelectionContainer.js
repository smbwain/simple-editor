import React from 'react';
import {Rect, Group} from 'react-konva';

import {RESIZE_BUTTON_SIZE} from '../../../consts';

const SelectionContainer = ({object, dragging, resizing}) => {
    if (!object) {
        return null;
    }
    const w = resizing ? object.w * resizing.coef : object.w;
    let h = resizing ? object.h * resizing.coef : object.h;
    return (
        <Group
            x={dragging ? dragging.x : object.x}
            y={dragging ? dragging.y : object.y}
        >
            <Rect
                data-id={object.id}
                width={w}
                height={h}
                stroke="red"
            />
            { object.type === 'logo' && (
                <Rect
                    x={w-RESIZE_BUTTON_SIZE/2}
                    y={h-RESIZE_BUTTON_SIZE/2}
                    width={RESIZE_BUTTON_SIZE}
                    height={RESIZE_BUTTON_SIZE}
                    stroke="red"
                    fill="white"
                    data-id="resizer"
                />
            ) }
        </Group>
    );
};

export default SelectionContainer;