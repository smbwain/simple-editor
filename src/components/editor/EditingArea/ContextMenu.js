/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';

import './ContextMenu.css';

const ContextDefault = ({onDelete, x, y}) => (
    <div
        className="ContextMenu"
        style={{
            left: x,
            top: y,
        }}
    >
        <ul>
            <li>
                <a onClick={onDelete}>Delete</a>
            </li>
        </ul>
    </div>
);

export default ContextDefault;