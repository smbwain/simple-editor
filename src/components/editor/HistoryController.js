import React from 'react';

import './HistoryController.css';

const HistoryController = ({history, onUndo}) => (
    <div className="HistoryController">
        <button disabled={!history.length} onClick={onUndo}>Undo</button>
    </div>
);

export default HistoryController;