/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import {Stage, Layer} from 'react-konva';

import './EditingArea.css';

import {EDITOR_AREA_HEIGHT, EDITOR_AREA_WIDTH, LOGO_DEFAULT_SIZE, LOGO_MAX_SIZE, LOGO_MIN_SIZE} from '../../consts';
import EmptyBackground from '../../img/empty_background.png';
import ContextMenu from './EditingArea/ContextMenu';
import LazyImage from './EditingArea/LazyImage';
import EditorElement from './EditingArea/EditorElement';
import SelectionContainer from './EditingArea/SelectionContainer';

const sqr = x => x * x;
const fitIntoBounds = (x, min, max) => Math.max(min, Math.min(max, x));

export default class EditingArea extends React.Component {
    state = {
        dragging: null,
        contextMenu: null,
        resizing: null,
    };
    getObjectById(id) {
        return this.props.data.objects.find(object => object.id === id);
    }
    render() {
        const {data: {objects, backgroundUrl}, selectedObjectId} = this.props;
        const {dragging, contextMenu, resizing} = this.state;
        const selectedObject = selectedObjectId && this.getObjectById(selectedObjectId);
        return (
            <div className="EditingArea">
                <h2>Simple Editor</h2>
                <div
                    className="EditingArea__stage-frame"
                    onDragOver={this.onExternalDragOver}
                    onDrop={this.onExternalDrop}
                >
                    <Stage
                        width={EDITOR_AREA_WIDTH}
                        height={EDITOR_AREA_HEIGHT}
                        onMouseDown={this.onMouseDown}
                        onMouseMove={this.onMouseMove}
                        onContextMenu={this.onContextMenu}
                    >
                        <Layer>
                            <LazyImage src={EmptyBackground}/>
                        </Layer>
                        <Layer ref={ref => this.workingAreaRef = ref}>
                            <LazyImage src={backgroundUrl} width={EDITOR_AREA_WIDTH} height={EDITOR_AREA_HEIGHT}/>
                            { objects.map(object => (
                                <EditorElement
                                    key={object.id}
                                    object={object}
                                    dragging={object.id === selectedObjectId && dragging}
                                    resizing ={object.id === selectedObjectId && resizing}
                                />
                            )) }
                        </Layer>
                        <Layer>
                            <SelectionContainer
                                object={selectedObject}
                                dragging={dragging}
                                resizing={resizing}
                            />
                        </Layer>
                    </Stage>
                    { contextMenu && (
                        <ContextMenu
                            onDelete={this.onDeleteSelected}
                            x={contextMenu.x}
                            y={contextMenu.y}
                        />
                    ) }
                </div>
                <div className="EditingArea__bottom-panel">
                    <a ref={ref => this.linkRef = ref}>
                        <button onClick={this.onDownloadAsImage}>Download as image</button>
                    </a>
                </div>
            </div>
        );
    }
    componentDidMount() {
        document.addEventListener('mouseup', this.onMouseUp);
    }
    componentWillUnmount() {
        document.removeEventListener('mouseup', this.onMouseUp)
    }
    onDownloadAsImage = () => {
        this.linkRef.download = 'image.png';
        this.linkRef.href = this.workingAreaRef.getLayer().toDataURL();
    };
    onMouseDown = (e) => {
        if (e.evt.button !== 0) {
            return;
        }
        const objectId = e.target.attrs['data-id'];
        if (objectId === 'resizer' && this.props.selectedObjectId) {
            const object = this.getObjectById(this.props.selectedObjectId);
            const fullSize = Math.sqrt(sqr(object.w) + sqr(object.h));
            this.setState({
                resizing: {
                    objectId: object.id,
                    offsetX: e.evt.layerX - object.x - object.w,
                    offsetY: e.evt.layerY - object.y - object.h,
                    coef: 1,
                    fullSize,
                    minCoef: Math.sqrt(2*sqr(LOGO_MIN_SIZE)) / fullSize,
                    maxCoef: Math.sqrt(2*sqr(Math.min(
                        LOGO_MAX_SIZE,
                        EDITOR_AREA_WIDTH - object.x,
                        EDITOR_AREA_HEIGHT - object.y
                    ))) / fullSize,
                    x: object.x,
                    y: object.y,
                },
            });
            return;
        }
        if (objectId !== this.props.selectedObjectId) {
            this.props.selectObject(objectId || null);
        }
        if (objectId) {
            const object = this.getObjectById(objectId);
            this.setState({
                dragging: {
                    objectId: object.id,
                    offsetX: e.evt.layerX - object.x,
                    offsetY: e.evt.layerY - object.y,
                    x: object.x,
                    y: object.y,
                    w: object.w,
                    h: object.h,
                },
                contextMenu: false,
            });
        } else {
            this.setState({
                contextMenu: false,
            });
        }
    };
    onMouseUp = (e) => {
        const {dragging, resizing} = this.state;
        if (dragging) {
            this.setState({
                dragging: null,
            });
            this.props.moveObject(
                dragging.objectId,
                dragging.x,
                dragging.y,
            );
        }
        if (resizing) {
            this.setState({
                resizing: null,
            });
            this.props.resizeObject(
                resizing.objectId,
                resizing.coef,
            )
        }
    };
    onMouseMove = (e) => {
        if (this.state.resizing) {
            this.setState(({resizing}) => {
                return {
                resizing: resizing && {
                    ...resizing,
                    coef:
                        fitIntoBounds(
                            Math.sqrt(
                                sqr(e.evt.layerX - resizing.offsetX - resizing.x) +
                                sqr(e.evt.layerY - resizing.offsetY - resizing.y)
                            ) / resizing.fullSize,
                            resizing.minCoef,
                            resizing.maxCoef,
                        ),
                }
            }});
        }
        if (this.state.dragging) {
            this.setState(({dragging}) => ({
                dragging: dragging && {
                    ...dragging,
                    x: fitIntoBounds(e.evt.layerX - dragging.offsetX, 0, EDITOR_AREA_WIDTH - dragging.w),
                    y: fitIntoBounds(e.evt.layerY - dragging.offsetY, 0, EDITOR_AREA_HEIGHT - dragging.h),
                },
            }));
        }
    };
    onContextMenu = (e) => {
        e.evt.preventDefault();
        const target = e.target;
        const objectId = target.attrs['data-id'];
        if (objectId !== this.props.selectedObjectId) {
            this.props.selectObject(objectId || null);
        }
        this.setState({
            contextMenu: objectId && {
                x: e.evt.layerX,
                y: e.evt.layerY,
            },
        });
    };
    onDeleteSelected = () => {
        this.setState({
            contextMenu: null,
        });
        this.props.removeObject(this.props.selectedObjectId);
    };
    onExternalDragOver = (e) => {
        e.preventDefault();
    };
    onExternalDrop = (e) => {
        const rect = e.currentTarget.getBoundingClientRect();
        this.props.createLogo(
            e.dataTransfer.getData('src'),
            Math.min(Math.max(e.clientX - rect.left - LOGO_DEFAULT_SIZE/2, 0), EDITOR_AREA_WIDTH - LOGO_DEFAULT_SIZE),
            Math.min(Math.max(e.clientY - rect.top - LOGO_DEFAULT_SIZE/2, 0), EDITOR_AREA_HEIGHT - LOGO_DEFAULT_SIZE),
        );
    };
}