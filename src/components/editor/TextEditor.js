import React from 'react';
import {Text} from 'konva';

import './TextEditor.css';

const FONTS = [
    {
        label: 'Arial',
        value: 'Arial',
    },
    {
        label: 'Times New Roman',
        value: 'Times New Roman',
    },
    {
        label: 'Open Sans',
        value: '\'Open Sans\', sans-serif',
    },
];

const measureText = (text, fontFamily) => {
    const instance = new Text({
        text,
        fontFamily,
        fontSize: 20,
    });
    return {
        w: instance.width(),
        h: instance.height(),
    }
};

export default class TextEditor extends React.Component {
    state = {
        text: '',
        fontFamily : FONTS[0].value,
    };
    render() {
        const {text, fontFamily} = this.state;
        return (
            <div className="TextEditor">
                <h2>Add text</h2>
                <form onSubmit={this.onSubmit}>
                    <input
                        type="text"
                        value={text}
                        onChange={this.onChangeText}
                    />
                    <ul>
                        { FONTS.map(font => (
                            <li key={font.value}>
                                <label
                                    style={{
                                        fontFamily: font.value,
                                    }}
                                >
                                    <input
                                        type="radio"
                                        checked={font.value === fontFamily}
                                        onChange={this.onSelectFont}
                                        value={font.value}
                                    />
                                    { font.label }
                                </label>
                            </li>
                        )) }
                    </ul>
                    <button>Add</button>
                </form>
            </div>
        );
    }
    onChangeText = (e) => {
        this.setState({
            text: e.currentTarget.value,
        });
    };
    onSelectFont = (e) => {
        this.setState({
            fontFamily: e.currentTarget.value,
        });
    };
    onSubmit = (e) => {
        e.preventDefault();
        const {text, fontFamily} = this.state;
        const {w, h} = measureText(text, fontFamily);
        this.props.onCreateText(text, fontFamily, w, h);
    };
}