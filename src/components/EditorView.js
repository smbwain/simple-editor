import React from 'react';

import BackgroundSelector from "./editor/BackgroundSelector";
import HistoryController from "./editor/HistoryController";
import LogoSelector from "./editor/LogoSelector";
import EditingArea from "./editor/EditingArea";
import TextEditor from "./editor/TextEditor";

import './EditorView.css';

const EditorView = (props) => {
    const {setBackground, data, createLogo, createText, history, undo} = props;
    return (
        <div className="EditorView">
            <div className="EditorView__left-panel">
                <BackgroundSelector onChange={setBackground} value={data.background}/>
            </div>
            <div>
                <EditingArea {...props}/>
            </div>
            <div className="EditorView__right-panel">
                <div>
                    <LogoSelector onCreateLogo={createLogo}/>
                    <TextEditor onCreateText={createText}/>
                </div>
                <HistoryController
                    history={history}
                    onUndo={undo}
                />
            </div>
        </div>
    );
};

export default EditorView;
