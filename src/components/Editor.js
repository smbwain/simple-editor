import {connect} from "react-redux";

import * as actionCreators from '../redux/actions';
import EditorView from "./EditorView";

const Editor = connect(
    (state) => state,
    actionCreators,
)(EditorView);

export default Editor;