import {LOGO_DEFAULT_SIZE} from '../consts';

export const setBackground = (backgroundUrl) => ({
    type: 'SET_BACKGROUND',
    backgroundUrl,
});

export const selectObject = (selectedObjectId) => ({
    type: 'SELECT_OBJECT',
    selectedObjectId,
});

export const createText = (text, font, w, h) => ({
    type: 'CREATE_OBJECT',
    objectId: Math.random().toString().slice(2),
    props: {
        type: 'text',
        text,
        font,
        w,
        h,
    },
});

export const createLogo = (imgUrl, x = 0, y = 0) => ({
    type: 'CREATE_OBJECT',
    objectId: Math.random().toString().slice(2),
    props: {
        type: 'logo',
        imgUrl,
        x,
        y,
        w: LOGO_DEFAULT_SIZE,
        h: LOGO_DEFAULT_SIZE,
    },
});

export const removeObject = (objectId) => ({
    type: 'REMOVE_OBJECT',
    objectId,
});

export const moveObject = (objectId, x, y) => ({
    type: 'MOVE_OBJECT',
    objectId,
    x,
    y,
});

export const resizeObject = (objectId, coef) => ({
    type: 'RESIZE_OBJECT',
    objectId,
    coef,
});

export const undo = () => ({
    type: 'UNDO',
});