import {HISTORY_RECORDS} from '../consts';

export default (state, action) => {
    if (state === undefined) {
        return {
            data: {
                objects: [],
                background: null,
            },
            history: [],
            selectedObjectId: null,
        };
    }
    if (action.type === 'SELECT_OBJECT') {
        return {
            ...state,
            selectedObjectId: action.selectedObjectId,
        };
    }
    if (action.type === 'UNDO') {
        const history = [...state.history];
        const data = history.pop();
        return {
            ...state,
            data,
            history,
            selectedObjectId: null,
        };
    }
    const history = [...state.history, state.data].slice(0, HISTORY_RECORDS);
    if (action.type === 'SET_BACKGROUND') {
        return {
            ...state,
            data: {
                ...state.data,
                backgroundUrl: action.backgroundUrl,
            },
            history,
        };
    }
    if (action.type === 'CREATE_OBJECT') {
        return {
            ...state,
            data: {
                ...state.data,
                objects: [
                    ...state.data.objects,
                    {
                        id: action.objectId,
                        x: 0,
                        y: 0,
                        w: 0,
                        h: 0,
                        ...action.props,
                    },
                ],
            },
            history,
            selectedObjectId: action.objectId,
        };
    }
    if (action.type === 'REMOVE_OBJECT') {
        return {
            ...state,
            data: {
                ...state.data,
                objects: state.data.objects.filter(object => object.id !== action.objectId),
            },
            history,
            selectedObjectId: (state.selectedObjectId === action.objectId) ? null : state.selectedObjectId,
        };
    }
    if (action.type === 'MOVE_OBJECT') {
        return {
            ...state,
            data: {
                ...state.data,
                objects: state.data.objects.map(object => {
                    if (object.id !== action.objectId) {
                        return object;
                    }
                    return {
                        ...object,
                        x: action.x,
                        y: action.y,
                    };
                }),
            },
            history,
        };
    }
    if (action.type === 'RESIZE_OBJECT') {
        return {
            ...state,
            data: {
                ...state.data,
                objects: state.data.objects.map(object => {
                    if (object.id !== action.objectId) {
                        return object;
                    }
                    return {
                        ...object,
                        w: object.w * action.coef,
                        h: object.h * action.coef,
                    };
                }),
            },
            history,
        };
    }
    return state;
};