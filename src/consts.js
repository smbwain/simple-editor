
export const EDITOR_AREA_WIDTH = 400;
export const EDITOR_AREA_HEIGHT = 400;
export const HISTORY_RECORDS = 5;

export const LOGO_DEFAULT_SIZE = 100;
export const LOGO_MIN_SIZE = 30;
export const LOGO_MAX_SIZE = 150;

export const RESIZE_BUTTON_SIZE = 14;