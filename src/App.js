import React, {Component} from 'react';
import {Provider} from 'react-redux';

import store from './redux/store';
import Editor from "./components/Editor";

import './App.css';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <Editor/>
                </div>
            </Provider>
        );
    }
}

export default App;
