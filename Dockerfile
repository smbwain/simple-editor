FROM node:8.11.1-slim
WORKDIR /prj
COPY package* /prj/
RUN npm install
COPY . /prj
RUN npm run build

FROM nginx:1.13.12
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=0 /prj/build /prj/build
EXPOSE 80