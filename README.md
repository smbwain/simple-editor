Instruction
-----------

To run it:

- Copy repository to your computer.
- Run
  ```
  npm i
  ```
  (make sure you have last verison on node.js)
- Run
  ```
  npm run build
  ```
  It runs dev-server on port 3000 and opens page in browser.
