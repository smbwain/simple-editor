user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       80;
        server_name  localhost;
        charset utf-8;

        # access_log  /var/log/nginx/log/host.access.log  main;

        root   /prj/build;
        index index.html;

        location = /service-worker.js {
            add_header Last-Modified $date_gmt;
            add_header Cache-Control "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0";
            try_files $uri =404;
        }

        location ~* \.(?:manifest|appcache|html?|xml|json)$ {
            add_header Last-Modified $date_gmt;
            add_header Cache-Control "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0";
            # expires -1;
            # access_log logs/static.log; # I don't usually include a static log
        }

        location ~* \.(?:css|js)$ {
            try_files $uri =404;
            expires 1y;
            access_log off;
            add_header Cache-Control "public";
        }

        # Any route containing a file extension (e.g. /devicesfile.js)
        location ~ ^.+\..+$ {
            try_files $uri =404;
            access_log off;
        }

        # Any route that doesn't have a file extension (e.g. /devices)
        location / {
            rewrite  ^.* "/index.html";
        }
    }
}